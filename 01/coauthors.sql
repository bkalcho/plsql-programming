select a.name
from authors a
where exists (
    select 1
    from authors_publications x1,
         authors_publications x2
    where x1.author_id = a.id
          and x1.title = x2.title
          and x2.author_id <> a.id )
order by a.name;