select a.id,
       a.name,
       p.title,
       p.written_date
from authors a,
     authors_publications p
where a.id = p.author_id
order by a.name,
         p.written_date,
         p.title;