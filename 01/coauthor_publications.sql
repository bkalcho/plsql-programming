select p.title,
       a.name
from authors a,
     authors_publications p
where a.id = p.author_id 
      and exists (
            select 1
            from authors_publications x
            where x.title = p.title
            and x.id <> p.id )
order by p.title,
         a.name;