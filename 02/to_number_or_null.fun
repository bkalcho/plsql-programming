CREATE OR REPLACE FUNCTION TO_NUMBER_OR_NULL (aiv_number IN varchar2)
return number is
/*
to_number_or_null.fun
by Bojan G. Kalicanin on 2016-07-11
An errorless to_number() method
*/

begin
    return to_number(aiv_number);
exception
    when OTHERS then
        return NULL;
end to_number_or_null;
/
@fe.sql to_number_or_null;