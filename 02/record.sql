rem record.sql
rem by Bojan G. Kalicanin on 2016-07-12
rem An anonymous PL/SQL procedure to demonstrate
rem the use of PL/SQL records

declare

TYPE name_record IS RECORD (
    first_name          WORKERS.first_name%TYPE,
    middle_name         WORKERS.middle_name%TYPE,
    last_name           WORKERS.last_name%TYPE );

TYPE name_table IS TABLE OF name_record
INDEX BY BINARY_INTEGER;

t_name              name_table;

begin
    t_name(1).first_name := 'JOHN';
    t_name(1).last_name := 'DOE';
    t_name(2).first_name := 'JANE';
    t_name(2).last_name := 'DOE';

    pl(t_name(1).last_name || ', ' || t_name(1).first_name);
    pl(t_name(2).last_name || ', ' || t_name(2).first_name);
end;
/