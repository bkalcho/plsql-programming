-- This is an nonymous procedure, so it has no name
DECLARE
/*
    You declare local cursors, variables, and methods here,
    but you do not need to have a declaration section.
*/
BEGIN
    -- You write your executable code here

    NULL; -- Ahhh, you've got to have at least one command!
EXCEPTION
    when NO_DATA_FOUND then
        raise_application_error(-20000,
            'Hey, This is in the exception-handling section!');
END;
/
-- the forward slash on a line by itself says execute this procedure