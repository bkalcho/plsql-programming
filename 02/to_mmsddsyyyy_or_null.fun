create or replace FUNCTION to_mmsddsyyyy_or_null (in_date IN varchar2)
return date is
/*
    to_mmsddsyyyy_or_null.fun
    by Bojan G. Kalicanin on 2016-07-11
    to_mmsddsyyyy_or_null: function that returns date in format MM/DD/YYYY or null
    if text string is not possible to convert in that date format.
*/
begin
    return to_date(in_date, 'MM/DD/YYYY');
exception
    when OTHERS then
        return NULL;
end to_mmsddsyyyy_or_null;
/
@fe.sql to_mmsddsyyyy_or_null;