create or replace PACKAGE BODY number_ as

/*
    number_.pkb
    by Bojan G. Kalicanin on 2016-07-11
    Package body implementing function that
    handdles conversion to NUMBER type.
*/

FUNCTION to_number_or_null(ain_number IN varchar2)
return number is

begin
    return to_number(ain_number);
exception
    when OTHERS then
        return NULL;
end to_number_or_null;


end number_;
/
@be.sql;