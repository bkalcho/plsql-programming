create or replace PROCEDURE pl (aiv_text IN varchar2) is
/*
    pl.rpc
    by Bojan G. Kalicanin on 2016-07-11
    pl(): procedure that is wrapper to the procedure SYS.DBMS_OUTPUT.put_line()
*/
begin
    SYS.DBMS_OUTPUT.put_line(aiv_text);
end pl;
/
@pe.sql pl;