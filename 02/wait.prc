create or replace PROCEDURE wait (ain_seconds IN number) is
/*
    wait.prc
    by Bojan G. Kalicanin on 2016-07-11
    wait(): wrapper function for SYS.DBMS_LOCK.sleep()
*/
begin
    SYS.DBMS_LOCK.sleep(ain_seconds);
end wait;
/
@pe.sql wait;