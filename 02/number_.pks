create or replace PACKAGE number_ as

/*
    number_.pks
    by Bojan G. Kalicanin on 2016-07-11
    Package specification which declares
    methods for handling number conversions.
*/

/*
    Returns the passed varchar2 as a number if it represents a number,
    otherwise, it returns NULL
*/

FUNCTION to_number_or_null(ain_number IN varchar2)
return number;

end number_;
/
@se.sql;